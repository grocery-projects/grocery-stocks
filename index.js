const express = require('express');
const app = express();
const router = express.Router();
const controller = require('./controller');
require('dotenv').config();
const bodyParser = require('body-parser');
var port = process.env.PORT || 3000;
app.use(bodyParser.json());

router.post('/product',function(req,res){
    controller.createProduct(req, res)
});

router.put('/product/:id',function(req,res){
    controller.updateProduct(req, res)
});

router.get('/product',function(req,res){
    controller.getProducts(req, res)
});

router.get('/product/:id',function(req,res){
    controller.getProduct(req, res)
});

router.get('/category',function(req,res){
    controller.getCategories(req, res)
});

//add the router
app.use('/', router);
app.listen(port);

console.log('Running at Port ' + port);