const Sequelize = require('sequelize');
const database = require('./database-config');

const Product = database.define('product', {
    barCode: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    quantity: {
        allowNull: false,
        type: Sequelize.INTEGER
    },
    taxFreePrice: {
        allowNull: false,
        type: Sequelize.DOUBLE
    },
    categoryId: {
        foreignKey: true,
        type: Sequelize.INTEGER
    },
    updatedAt: {
        type: Sequelize.DATE,
    },
    createdAt: {
        type: Sequelize.DATE,
    }
}, {
    // options
});

module.exports = Product;
