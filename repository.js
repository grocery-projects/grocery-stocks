const Product = require('./product');
const Category = require('./category');
const Error = require('./error');
const { Op } = require("sequelize");

module.exports = {
    getProduct: function (id) {
        return new Promise((resolve, reject) => {
            Product.findOne({
                where: {
                    barCode: {
                        [Op.like]: `%${id}%`,
                    },
                }
            })
                .then(products => {
                    if(products != null) {
                        resolve(products);
                    } else {
                        resolve(products)
                    }
                })
                .catch(error => {
                    reject(error);
                })
        })
    },
    getProductsLike: function (barCode) {
        return new Promise((resolve, reject) => {
            Product.findAll({
                where: {
                    barCode: {
                        [Op.like]: `%${barCode}%`,
                    }
                }
            })
                .then(products => {
                    resolve(products);
                })
                .catch(error => {
                    reject(error);
                })
        })
    },
    getProducts: function () {
        return new Promise((resolve, reject) => {
            Product.findAll()
                .then(products => {
                    resolve(products);
                })
                .catch(error => {
                    reject(error);
                })
        })
    },
    createProduct: function(barCode, categoryId, name, taxFreePrice, quantity) {
        return new Promise((resolve, reject) => {
            return Product.create(
                {
                    barCode: barCode,
                    categoryId: categoryId,
                    name: name,
                    taxFreePrice:
                    taxFreePrice,
                    quantity: quantity
                })
                .then(result => {
                    resolve(result);
                })
                .catch(error => {
                    reject(error);
                })
        })
    },
    updateProduct: function(barCode, name, taxFreePrice, quantity) {
        return new Promise((resolve, reject) => {
            return Product.update(
                {name: name, taxFreePrice: taxFreePrice, quantity: quantity},
                {where: {barCode: barCode}}
            )
                .then(result => {
                    if(result[0] !== 0) {
                        resolve();
                    }
                    reject(Error.NOT_FOUND);
                })
                .catch(error => {
                    reject(error);
                })
        })
    },
    getCategories: function() {
        return new Promise((resolve, reject) => {
            return Category.findAll()
                .then(products => {
                    resolve(products);
                })
                .catch(error => {
                    reject(error);
                })
        })
    }
};