const Sequelize = require('sequelize');
const database = require('./database-config');

const Category = database.define('category', {
    id: {
        primaryKey: true,
        type: Sequelize.INTEGER,
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    tax: {
        allowNull: true,
        type: Sequelize.DOUBLE
    },
    updatedAt: {
        type: Sequelize.DATE,
    },
    createdAt: {
        type: Sequelize.DATE,
    }
}, {
    // options
});

module.exports = Category;
