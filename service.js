const repository = require('./repository');

module.exports = {
    getProduct: function (id) {
        return new Promise((resolve, reject) => {
            return repository.getProduct(id)
                .then((result) => {
                    resolve(result)
                })
                .catch((error) => {
                    reject(error);
                })
        })

    },
    getProducts: function (barCode) {
        if(barCode === undefined) {
            return new Promise((resolve, reject) => {
                return repository.getProducts()
                    .then((result) => {
                        resolve(result)
                    })
                    .catch((error) => {
                        reject(error);
                    })
            })
        } else {
            return new Promise((resolve, reject) => {
                return repository.getProductsLike(barCode)
                    .then((result) => {
                        resolve(result)
                    })
                    .catch((error) => {
                        reject(error);
                    })
            })
        }
    },
    updateProduct: function(barCode, name, taxFreePrice, quantity) {
        return new Promise((resolve, reject) => {
            return repository.updateProduct(barCode, name, taxFreePrice, quantity)
                .then(() => {
                    resolve()
                })
                .catch((error) => {
                    reject(error);
                })
        })
    },
    createProduct: function(barCode, categoryId, name, taxFreePrice, quantity) {
        return new Promise((resolve, reject) => {
            return repository.createProduct(barCode, categoryId, name, taxFreePrice, quantity)
                .then((result) => {
                    resolve(result)
                })
                .catch((error) => {
                    reject(error);
                })
        })
    },
    getCategories: function() {
        return new Promise((resolve, reject) => {
            return repository.getCategories()
                .then((result) => {
                    resolve(result)
                })
                .catch((error) => {
                    reject(error);
                })
        })
    }
};