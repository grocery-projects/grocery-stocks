const service = require('./service');
const Sequelize = require('sequelize');
const Error = require('./error');

module.exports = {
    createProduct: function(req, res) {
        service.createProduct(req.body.barCode, req.body.categoryId, req.body.name, req.body.taxFreePrice, req.body.quantity)
            .then(result => {
                res.status(201).send(JSON.stringify(result, null, 4))
            })
            .catch((error) => {
                console.log('error: ' + error);
                if(error instanceof Sequelize.UniqueConstraintError) {
                    res.status(409).send(error)
                } else {
                    res.status(500).send(error)
                }
            })
    },
    updateProduct: function(req, res) {
        service.updateProduct(req.params.id, req.body.name, req.body.taxFreePrice, req.body.quantity)
            .then(result => {
                res.status(204).send()
            })
            .catch((error) => {
                if(error === Error.NOT_FOUND) {
                    res.status(400).send({'reason': 'Product not found'})
                } else {
                    res.status(500).send(error)
                }
            })
    },
    getProduct: function (req, res) {
        service.getProduct(req.params.id)
            .then((result) => {
                if(result === null) {
                    console.log(result);
                    res.status(400).send({ 'error': 'Product Not found'})
                } else {
                    res.status(200).send(JSON.stringify(result, null, 4))
                }
            })
            .catch((error) => {
                res.status(500).send(error)
            })
    },
    getProducts: function (req, res) {
        service.getProducts(req.query.barCode)
            .then((result) => {
                res.status(200).send(JSON.stringify(result, null, 4))
            })
            .catch((error) => {
                res.status(500).send(error)
            })
    },
    getCategories: function(req, res) {
        service.getCategories()
            .then((result) => {
                res.status(200).send(JSON.stringify(result, null, 4))
            })
            .catch((error) => {
                res.status(500).send(error)
            })
    }
};